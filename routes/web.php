<?php

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/', 'DashController');
    Route::get('/logout')->name('logout')->uses('Auth\LoginController@logout');

    //ver la pagina donde estan los usuarios
    Route::get('/libros', 'LibrosController@index');

    //mostrar formulario de usuario nuevo
    Route::get('/libros/create', 'LibrosController@create');

    //guardar usuario nuevo
    Route::post('/libros', 'LibrosController@store');

    //mostrar el formulario de actualizar usuario
    Route::get('/libros/{id}/edit' , 'LibrosController@edit');

    //guardar el usuario actualizado
    Route::put('/libros/{id}/', 'LibrosController@uptade');

    //eliminar usuario
    Route::delete('/libros/{id}' , 'LibrosController@destroy');
});

