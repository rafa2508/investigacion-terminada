<?php

namespace App\Http\Controllers;

use App\Models\libros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class LibrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        $libros = libros::all();

        return Inertia::render('Libros/Index', [
           'libros' => $libros
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Libros/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        DB::beginTransaction();
        $libros = new Libros();
        $libros->nombre = $request->nombre;
        $libros->publicadora = $request->publicadora;
        $libros->pais = $request->pais;
        $libros->fecha = $request->fecha;
        $libros->paginas = $request->paginas;
        $libros->capitulos = $request->capitulos;
        $libros->save();
        DB::commit();

        return redirect('/Libros');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function edit($id)
    {
        $libros = Libros::where('id', $id)->first();

        return Inertia::render('Libros/Edit', [
            'libros' => $libros
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $libros = Libros::where('id', $id)->first();
        $libros->nombre = $request->nombre;
        $libros->publicadora = $request->publicadora;
        $libros->pais = $request->pais;
        $libros->fecha = $request->fecha;
        $libros->paginas = $request->paginas;
        $libros->capitulos = $request->capitulos;
        $libros->save();
        DB::commit();

        return redirect('/Libros');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        $libros = Libros::where('id', $id)->first();
        $libros->delete();
        DB::commit();

        return redirect('/Libros');
    }

}
