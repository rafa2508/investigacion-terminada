(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Libros/Create.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Libros/Create.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {},
  data: function data() {
    return {
      form: {
        valid: true,
        nombre: '',
        publicadora: '',
        pais: '',
        fecha: '',
        paginas: '',
        capitulos: ''
      },
      rules: [function (v) {
        return !!v || 'Campo Requerido.';
      }]
    };
  },
  props: {
    source: String
  },
  methods: {
    submit: function submit() {
      this.$inertia.post('/libros', {
        nombre: this.form.nombre,
        publicadora: this.form.publicadora,
        pais: this.form.pais,
        fecha: this.form.fecha,
        paginas: this.form.paginas,
        capitulos: this.form.capitulos
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Libros/Create.vue?vue&type=template&id=fa45a83e&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Libros/Create.vue?vue&type=template&id=fa45a83e& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "VLayout",
    [
      _c(
        "v-toolbar",
        [
          _c("span", { staticStyle: { "font-size": "3em" } }, [
            _c("i", {
              staticClass: "fa fa-book fa-lg",
              attrs: { "aria-hidden": "true" }
            }),
            _vm._v(" Nuevo Libro")
          ]),
          _vm._v(" "),
          _c("v-spacer"),
          _vm._v(" "),
          _c("v-btn", { attrs: { rounded: "", dark: "", href: "/libros" } }, [
            _vm._v("Salir del Formulario")
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-container",
        { staticClass: "login" },
        [
          _c(
            "v-card",
            { staticClass: "mx-auto", attrs: { outlined: "" } },
            [
              _c(
                "v-toolbar",
                { attrs: { dark: "" } },
                [
                  _c(
                    "v-container",
                    [
                      _c(
                        "v-toolbar-title",
                        { staticStyle: { "font-size": "3em" } },
                        [_vm._v("Nuevo")]
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-container",
                [
                  _c("v-card-text", [
                    _c(
                      "form",
                      {
                        ref: "form",
                        model: {
                          value: _vm.form.valid,
                          callback: function($$v) {
                            _vm.$set(_vm.form, "valid", $$v)
                          },
                          expression: "form.valid"
                        }
                      },
                      [
                        _c("v-text-field", {
                          attrs: {
                            type: "text",
                            name: "nombre",
                            rules: _vm.rules,
                            label: "Nombre"
                          },
                          model: {
                            value: _vm.form.nombre,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "nombre", $$v)
                            },
                            expression: "form.nombre"
                          }
                        }),
                        _vm._v(" "),
                        _c("v-text-field", {
                          attrs: {
                            type: "text",
                            name: "publicadora",
                            rules: _vm.rules,
                            label: "Publicadora"
                          },
                          model: {
                            value: _vm.form.publicadora,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "publicadora", $$v)
                            },
                            expression: "form.publicadora"
                          }
                        }),
                        _vm._v(" "),
                        _c("v-text-field", {
                          attrs: {
                            type: "text",
                            name: "pais",
                            rules: _vm.rules,
                            label: "Pais de Origen"
                          },
                          model: {
                            value: _vm.form.pais,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "pais", $$v)
                            },
                            expression: "form.pais"
                          }
                        }),
                        _vm._v(" "),
                        _c("v-text-field", {
                          attrs: {
                            type: "text",
                            name: "fecha",
                            rules: _vm.rules,
                            label: "Fecha Publicado"
                          },
                          model: {
                            value: _vm.form.fecha,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "fecha", $$v)
                            },
                            expression: "form.fecha"
                          }
                        }),
                        _vm._v(" "),
                        _c("v-text-field", {
                          attrs: {
                            type: "text",
                            name: "paginas",
                            rules: _vm.rules,
                            label: "Cantidad de Paginas"
                          },
                          model: {
                            value: _vm.form.paginas,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "paginas", $$v)
                            },
                            expression: "form.paginas"
                          }
                        }),
                        _vm._v(" "),
                        _c("v-text-field", {
                          attrs: {
                            type: "text",
                            name: "capitulos",
                            rules: _vm.rules,
                            label: "Cantidad de Capitulos"
                          },
                          model: {
                            value: _vm.form.capitulos,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "capitulos", $$v)
                            },
                            expression: "form.capitulos"
                          }
                        })
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { attrs: { align: "center" } },
                    [
                      _c(
                        "v-btn",
                        {
                          attrs: { rounded: "", dark: "" },
                          on: {
                            click: function($event) {
                              return _vm.submit()
                            }
                          }
                        },
                        [_vm._v("Guardar")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/Pages/Libros/Create.vue":
/*!**********************************************!*\
  !*** ./resources/js/Pages/Libros/Create.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Create_vue_vue_type_template_id_fa45a83e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Create.vue?vue&type=template&id=fa45a83e& */ "./resources/js/Pages/Libros/Create.vue?vue&type=template&id=fa45a83e&");
/* harmony import */ var _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Create.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Libros/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Create_vue_vue_type_template_id_fa45a83e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Create_vue_vue_type_template_id_fa45a83e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Libros/Create.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Libros/Create.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/Pages/Libros/Create.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Libros/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Libros/Create.vue?vue&type=template&id=fa45a83e&":
/*!*****************************************************************************!*\
  !*** ./resources/js/Pages/Libros/Create.vue?vue&type=template&id=fa45a83e& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_fa45a83e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=template&id=fa45a83e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Libros/Create.vue?vue&type=template&id=fa45a83e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_fa45a83e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_fa45a83e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);